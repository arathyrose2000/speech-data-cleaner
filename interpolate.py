#!/usr/bin/python3
import json
import sys
import json
from text2digits import text2digits
t2d = text2digits.Text2Digits()

usage = ''' Expected usage:
python3 interpolate.py <main.json> <num.json>
For example: python3 interpolate.py good.json bad.json
If the files are not exactly json, pls fix it manually
'''
# options
json.encoder.FLOAT_REPR = lambda f: ("%.3f" % f)
isPrint = False
isCommandLine = 1
goodFileName = "good.json"
badFileName = "bad.json"
modelOutputFile = "out.json"
error_threshold = 0.4
numberOfWords = 7
subs = {
    'q one': 'Q1', 'q 1': 'Q1',
    'q two': 'Q2', 'q 2': 'Q2',
    'q three': 'Q3', 'q 3': 'Q3',
    'q four': 'Q4', 'q 4': 'Q4',
    '2000 and 21': '2021',
    '2000 and 20': '2020',
    '2000 and 22': '2022',
    '2000 and 24': '2024',
}

# getting file names
if isCommandLine == 1:
    try:
        goodFileName = sys.argv[1]
        badFileName = sys.argv[2]
    except:
        print("Incorrect usage")
        print(usage)
        print()
        exit(2)

goodAll = json.load(open(goodFileName))
badAll = json.load(open(badFileName))
combinedAll = []
saveToFile = True

for si in range(len(goodAll)):
    current_word = goodAll[si]
    surrounding_words = [goodAll[si+j]["word"]
                         for j in range(-1*numberOfWords-1, numberOfWords) if si+j < len(goodAll) and si+j >= 0 and j != 0]
    if current_word["word"] == "<unk>":
        replacewordwith = ""
        allwordsinrange = ""
        for j in badAll:
            if j['start'] >= current_word['start'] - error_threshold and j['end'] <= current_word['end'] + error_threshold:
                if j["word"] not in surrounding_words: # and t2d.convert(j["word"]).isnumeric():
                    replacewordwith += j["word"] + " "
                allwordsinrange += j["word"]
        if isPrint:
            print(si, "unk -- (", current_word["start"], current_word["end"], ")",
                  [goodAll[si+j]["word"]
                      for j in range(-1*numberOfWords-1, numberOfWords) if si+j < len(goodAll) and si+j >= 0],
                  " -- replaced with -- ", replacewordwith, " -- ", t2d.convert(replacewordwith),
                  " -- selected from the set of words: -- ", allwordsinrange)
        current_word["word"] = t2d.convert(replacewordwith).strip()
    combinedAll.append(current_word)

if not isPrint:
    print(json.dumps(combinedAll, indent=4, separators=(',', ': ')))
else:
    with open(modelOutputFile, "w") as f:
        json.dump(combinedAll, f, indent=4, separators=(',', ': '))
