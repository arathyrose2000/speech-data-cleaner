from rpunct import RestorePuncts
import sys
# The default language is 'en'
rpunct = RestorePuncts()

usage = ''' Expected usage:
python3 punctuatorb.py <input.txt> <output.txt>
For example: python3 punctuator.py out.txt finalOut.txt
'''

isCommandLine = True
inputFile = "out.txt"
outputFile = "finalOut.txt"

if isCommandLine:
    try:
        inputFile = sys.argv[1]
        if len(sys.argv) > 2:
            outputFile = sys.argv[2]
        else:
            outputFile = None
    except:
        print("Incorrect usage")
        print(usage)
        print()
        exit(2)

with open(inputFile, "r") as f:
    lines = f.readlines()
    for l in lines:
        if l == '\n': continue
        punctuatedLine = rpunct.punctuate(l, lang='en')
        print(punctuatedLine)
        if outputFile:
            open(outputFile, 'w').write(punctuatedLine)
