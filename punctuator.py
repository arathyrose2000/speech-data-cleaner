from fastpunct import FastPunct
import sys
# The default language is 'en'
fastpunct = FastPunct('English')

usage = ''' Expected usage:
python3 punctuator.py <input.txt> <output.txt>
For example: python3 punctuator.py out.txt finalOut.txt
'''

isCommandLine = True
inputFile = "out.txt"
outputFile = "finalOut.txt"

if isCommandLine:
    try:
        inputFile = sys.argv[1]
        if len(sys.argv) > 2:
            outputFile = sys.argv[2]
        else:
            outputFile = None
    except:
        print("Incorrect usage")
        print(usage)
        print()
        exit(2)

with open(inputFile, "r") as f:
    lines = f.readlines()
    for l in lines:
        punctuatedLine = fastpunct.punct(l, correct=False)
        print(punctuatedLine)
        if outputFile:
            open(outputFile, 'w').write(punctuatedLine)
