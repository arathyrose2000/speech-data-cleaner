#!/usr/bin/python3

import sys
import io
import json

usage = ''' Expected usage:
cat <batch decoder output> | python3 makeJson.py <word-id file> <rttm file> 
For example: cat transcript.txt | python3 makeJson.py words.txt rttm
'''

# options
json.encoder.FLOAT_REPR = lambda f: ("%.3f" % f)
isPrint = 0
isCommandLine = 1
errThreshold = 0

wordFile = "words.txt"
speakerRTTMFile = "rttm"
transcriptFile = "transcript.txt"
segFile = "seg"
modelOutputFile = "out.json"

# getting file names
if isCommandLine == 1:
    try:
        wordFile = sys.argv[1]
        speakerRTTMFile = sys.argv[2]
        if len(sys.argv) > 3:
            segFile = sys.argv[3]
        else:
            segFile = None
        if len(sys.argv) > 4:
            transcriptFile = sys.argv[4]
        else:
            transcriptFile = None
    except:
        print("Incorrect usage")
        print(usage)
        print()
        exit(2)

# Get a list of all words in the word id file
allWordsText = open(wordFile, encoding='utf-8').readlines()
allWords = {}
for wline in allWordsText:
    wline = wline.split(' ')
    allWords[wline[1].strip()] = wline[0]

# get segment start time and end time
if segFile:
    segText = open(segFile, encoding='utf-8').readlines()
    allSegs = {}
    for sline in segText:
        sline = sline.split(' ')
        allSegs[sline[0].strip()] = {"first": float(sline[2]), "last": float(sline[3]), "file": sline[1]}

# Get a list of all speakers as an array, sorted by their start time
speakers = []
allSpeakersRTTM = open(speakerRTTMFile, encoding='utf-8').readlines()
for line in allSpeakersRTTM:
    # remove unnecessary spaces between the items
    line = ' '.join(line.split())
    # SPEAKER file0      1          3553.990       6.260 <NA> <NA> 6            <NA> <NA>
    # SPEAKER <fileid> <channel> <start time> <duration> <NA> <NA> <Speaker id> <NA> <NA>
    _, fileID, channel, start, duration, _, _, speaker_id, _, _ = line.split(
        ' ')
    speakers.append({"speaker": speaker_id, "fileID": fileID, "channel": channel, "start": float(
        start), "duration": float(duration), "end": float(start)+float(duration)})

# sort utts based on their starting time
speakers.sort(key=lambda tup: -tup["start"])
if isPrint:
    print(speakers[0])
if isPrint:
    print("Speakers talking are as follows:")
    for sp in speakers:
        print(sp['speaker'], ":", sp["start"], sp["end"])


# a function to get which speaker is talking that word with given start_time and end_time (in float)
# returns -1 if no speaker is found
def getSpeaker(start_time, end_time):
    for sp in speakers:
        sp_startTime = sp["start"] - errThreshold
        sp_endTime = sp["end"] + errThreshold
        if start_time >= sp_startTime and end_time <= sp_endTime:
            #if isPrint: print(sp["speaker"], start_time, end_time, sp_startTime, sp_endTime)
            return sp["speaker"]
    return -1


# get utterance source
if transcriptFile == None:
    uttLines = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
else:
    uttLines = open('transcript.txt', 'r').readlines()
# get and fix utterances
utts = []
for line in uttLines:
    # remove unnecessary spaces between the items
    line = ' '.join(line.split())
    # <fileid>-<from>-<to> <channel>  <start time> <duration> <word id> <confidence>
    # file0-0057100-0057368 1             0.72         0.06      307699 0.57
    part1, channel, start, duration, wordID, conf = line.split(' ')
    if segFile:
        segInfo = allSegs[part1]
        fileID = segInfo["file"]
        first = segInfo["first"]
        last = segInfo["last"]
    else:
        segInfo = part1.split('-')
        fileID = segInfo[0]
        first = float(segInfo[1])/100
        last = float(segInfo[2])/100
    word = allWords[wordID]
    #if part1 not in utts: utts[part1] = []
    #print(first, last)
    startTime = first + float(start)
    endTime = first + float(start) + float(duration)
    utts.append({
                 "ID": part1, "fileID": fileID, "channel": channel, "word": word,
                 "start": startTime,
                 "duration": float(duration),
                 "end": endTime,
                 "confidence": float(conf),
                 "speaker": getSpeaker(startTime, endTime)
                })
# sort utts based on their starting time
utts.sort(key=lambda tup: tup["start"])

if not isPrint:
    print(json.dumps(utts, indent=4, separators=(',', ': ')))

else:
    with open(modelOutputFile, "w") as f:
        json.dump(utts, f, indent=4, separators=(',', ': '))
    textPart = ""
    # just the text of utterances based on user id
    prev_speaker = -1
    for u in utts:
        if prev_speaker == u["speaker"]:
            if isPrint:
                print(u["word"], end=" ")
            textPart += u["word"]+" "
        else:
            prev_speaker = u["speaker"]
            if isPrint:
                print("\n" + str(prev_speaker)+": " + u["word"], end=" ")
            textPart += "\n"+u["word"]+" "

    if isPrint:
        for param in speakers[0]:
            print("All "+param+" detected = ",
                set(list([sp[param] for sp in speakers])))
            print("All "+param+" with words = ",
                set(list([u[param] for u in utts])))

