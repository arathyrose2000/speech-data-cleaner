# Speech data cleaner

Cleans the speech by performing the following operations
1. Get the utterances of a model
2. Create a utt.json file for the model
3. Interpolate utterances from two models, one with <unk> in place of numbers, and the other that parses numbers but has an overall lower accuracy
4. Convert the number words into numbers
5. Punctuate the sentences and overall fix the sentences in the files

## Files included and their explanations

### File 1: makeJSON.py

This file converts the output of the model into a list of utterence objects with speaker information attached to it. The output will be displayed / sent to stdout.

#### Sample usage:
```bash
cat <batch decoder output> | python3 makeJson.py <word-id file> <rttm file> <seg file>
```

#### For example: `cat transcript.txt | python3 makeJson.py words.txt rttm seg`

#### Notes:
- For debugging, please edit the code here: `isprint = True`. In this case, the output will be stored in the modelOutputFile (which can be set within the code, default `out.json`). 
- If no seg is given, the segments are 'guessed' from the fileID-X-Y as X/100-->segment start and Y/100 -->segment end. All other arguments are compulsory.

### interpolate.py

This file combines the output of two models, one with <unk> and the other with numbers and prints the combined utterances

#### Sample usage:
```bash 
python3 interpolate.py <main.json> <num.json>
```

#### For example: python3 interpolate.py good.json bad.json

#### Notes:
- For debugging, please edit the code here: `isprint = True`. In this case, the output will be stored in the modelOutputFile (which can be set within the code, default `out.json`). 

### generateTxt.py

This file converts the utterances into meaningful text, with words spoken by each speaker on a separate line. The output will be displayed / sent to stdout.

#### Sample usage:
```bash
python3 generateTxt.py <json file>
```
#### For example: python3 generateTxt.py modelTxt.json

### punctuatorb.py

This file uses [`rpunct`](https://github.com/Felflare/rpunct/) to punctuate the output obtained from the previous step.

#### Usage
Before using this file, please do the following steps

1. Install rpunct using the command `!pip install rpunct`
2. If you are not using a GPU, please follow the fix as mentioned [here](https://github.com/Felflare/rpunct/issues/1). If in case you are unable to find the location of `ner_model.py`, use `find / | grep simpletransformers/ner/ner_model.py` and find the location of the file, and then change the value of `set_cuda=False` at/around line 114
3. Run the file as `python3 punctuatorb.py <input text file> <output text file>`

#### Sample usage:
```bash
python3 punctuatorb.py <input text file> <output text file>
```
