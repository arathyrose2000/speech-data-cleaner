import sys
import io
import json

usage = ''' Expected usage:
python3 generateTxt.py <json file>
Example: python3 generateTxt.py modelTxt.json
'''
isPrint = False
threshold = 20

# load file
try:
    utts = json.load(open(sys.argv[1]))
except:
    print("Invalid format!")
    print(usage)
    exit(2)
    
textPart = ""
numW = 0

# get speaker utterences
allUtterencesBySpeaker = []
wordCounts = []
prev_speaker = -1
currentSentence = ""
for u in utts:
    if prev_speaker != u["speaker"] and u["speaker"]!=-1:
        prev_speaker = u["speaker"]
        if isPrint:
            print("\n" + str(prev_speaker)+": " + u["word"], end=" ")
        allUtterencesBySpeaker.append(currentSentence)
        wordCounts.append(numW)
        numW = 0
        currentSentence = ""
    numW+=1
    currentSentence += u["word"] +" "

allUtterencesBySpeaker.append(currentSentence)
wordCounts.append(numW)

# print(allUtterencesBySpeaker, wordCounts)

# for long to short
'''
textPart = ""
for i in range(len(allUtterencesBySpeaker)):
    if wordCounts[i]>threshold:
        textPart += "\n"+ allUtterencesBySpeaker[i]
    else:
        textPart += allUtterencesBySpeaker[i]
'''

# for short to long
textPart = ""
threshold = 10
for i in range(len(allUtterencesBySpeaker)):
    if wordCounts[i]<threshold:
        textPart += allUtterencesBySpeaker[i]+ " "
    else:
        textPart += "\n"+allUtterencesBySpeaker[i]


print(textPart)
